package beanmanager.admin;

import entity.Sach;
import beanmanager.util.JsfUtil;
import beanmanager.util.JsfUtil.PersistAction;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import sessionbean.SachFacade;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named("sachController")
@SessionScoped
public class SachController implements Serializable {

    @EJB
    private sessionbean.SachFacade ejbFacade;
    private List<Sach> items = null;
    private Sach selected;
    private UploadedFile anh;
    private UploadedFile file;

    public void handleAnhUpload(FileUploadEvent event) {
        anh = event.getFile();
        selected.setTenanh(event.getFile().getFileName());
    }

    public void handleFileUpload(FileUploadEvent event) {
        file = event.getFile();
        selected.setTenfile(event.getFile().getFileName());

    }

    public UploadedFile getAnh() {
        return anh;
    }

    public void setAnh(UploadedFile anh) {
        this.anh = anh;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {

        this.file = file;
    }

    public SachController() {
    }

    public Sach getSelected() {
        return selected;
    }

    public void setSelected(Sach selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private SachFacade getFacade() {
        return ejbFacade;
    }

    public Sach prepareCreate() {
        selected = new Sach();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {

        if (file != null && anh != null) {
            FileOutputStream fos = null;
            try {
                String imgPath = null;
                imgPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/images/");//   
                fos = new FileOutputStream(new File(imgPath + "/" + anh.getFileName()));
                InputStream is = anh.getInputstream();
                int BUFFER_SIZE = 8192;
                byte[] buffer = new byte[BUFFER_SIZE];
                int a;
                while (true) {
                    a = is.read(buffer);
                    if (a < 0) {
                        break;
                    }
                    fos.write(buffer, 0, a);
                    fos.flush();
                }
                fos.close();
                is.close();

            } catch (FileNotFoundException ex) {
                Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            FileOutputStream fos1 = null;
            try {
                String filePath = null;
                filePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/files/");//   
                fos1 = new FileOutputStream(new File(filePath + "/" + file.getFileName()));
                InputStream is1 = file.getInputstream();
                int BUFFER_SIZE = 8192;
                byte[] buffer = new byte[BUFFER_SIZE];
                int a;
                while (true) {
                    a = is1.read(buffer);
                    if (a < 0) {
                        break;
                    }
                    fos1.write(buffer, 0, a);
                    fos1.flush();
                }
                fos1.close();
                is1.close();

            } catch (FileNotFoundException ex) {
                Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos1.close();
                } catch (IOException ex) {
                    Logger.getLogger(SachController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        selected.setTenanh(anh.getFileName());
        selected.setTenfile(file.getFileName());
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("SachCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("SachUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("SachDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Sach> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {

            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Sach getSach(java.lang.String id) {
        return getFacade().find(id);
    }

    public List<Sach> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Sach> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Sach.class)
    public static class SachControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SachController controller = (SachController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "sachController");
            return controller.getSach(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Sach) {
                Sach o = (Sach) object;
                return getStringKey(o.getMaSach());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Sach.class.getName()});
                return null;
            }
        }

    }

}
