/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanmanager.client;

import entity.Sach;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import sessionbean.SachFacade;

/**
 *
 * @author Trong Tran
 */
@Named(value = "clienController")
@SessionScoped
public class ClienController implements Serializable {

    /**
     * Creates a new instance of ClienController
     */
    public ClienController() {
    }
    @EJB
    private SachFacade sachFacade;
    private Sach book;
    private List<Sach> listBooks;
    private StreamedContent file;
    String keyword = "";
    
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public StreamedContent getFile() {
        InputStream iStream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/files/" + book.getTenfile());
        file = new DefaultStreamedContent(iStream, "application/pdf", book.getTenfile());
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public Sach getBook() {
        return book;
    }

    public void setBook(Sach book) {
        this.book = book;
    }

    public void setListBooks(List<Sach> listBooks) {
        this.listBooks = listBooks;
    }

    public List<Sach> getListBooks() {
        listBooks = sachFacade.getBook(keyword);
        return listBooks;
    }
}
