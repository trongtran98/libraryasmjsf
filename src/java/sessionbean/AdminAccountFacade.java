/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entity.AdminAccount;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Trong Tran
 */
@Stateless
public class AdminAccountFacade extends AbstractFacade<AdminAccount> {

    @PersistenceContext(unitName = "WebApplication2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminAccountFacade() {
        super(AdminAccount.class);
    }
    
    public AdminAccount getAccount(AdminAccount account){
        Query query = em.createNamedQuery("AdminAccount.findByUsernameAndPassword");
        query.setParameter("username", account.getUsername());
        query.setParameter("password", account.getPassword());
        
        return (AdminAccount) query.getSingleResult();
    }
}
